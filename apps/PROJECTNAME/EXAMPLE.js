db.sessions.drop();
db.users.drop();
db.fugitives.drop();
db.links.drop();
db.operators.drop();

db.fugitives.insert( {
	nickname : 'Brooke'
	, name : { first : 'Brooke' , last : 'Burukku' } , 
	age : 90,
	email : 'burukku@brooke.com' ,
	passwordHash : 'qwerty' , 
	sightings : [ 
		{ at : new Date( 2004 , 3, 4, 5, 6, 7 ) , minutes : 12 },
		{ at : new Date( 2003 , 3, 4, 5, 6, 7 ) , minutes : 2 },
		{ at : new Date( 2002 , 3, 4, 5, 6, 7 ) , minutes : 6 }
	] 
	, gender: 'male'
	, location: 'Grand Line'
} );

var f9 = db.fugitives.findOne( { 'name.first' : 'Brooke'} );

db.links.insert( {
	title : 'Humming Brook' ,  
	url : 'http://onepiece.wikia.com/wiki/Brook' , 
	comment : 'Musician' , 
	tags : [ 'mugiwara', 'musician' , 'women' ] , 
	favorites : 245 , 
	userId : f9._id 
} );

