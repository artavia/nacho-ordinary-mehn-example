( () => {
  
  // ~/delta/devt/mongo-atlas/ntv-perf-focus-best-prax-expressjs/custom_dbs/alpha-mongo_client.js

  /* ######################################
  ##  additional package imports
  ###################################### */
  const MongoClient = require('mongodb').MongoClient;

  /* ######################################
  ##  See grouping in package.json
  ##  where the the ID data is decoupled for this sheet
  ##  // , "config" : { "port" : "8080" }
  ##  // http.createServer(...).listen( process.env.npm_package_config_port )
  ###################################### */

  // database specific information & Database's name
  const DATABASE_NAME = "PROJECTNAME"; 
  
  const { CONNECTION_URL } = process.env;

  let cachedDb = null; // from https://docs.atlas.mongodb.com/best-practices-connecting-to-aws-lambda/#example 

  const connect = ( url ) => {

    console.log('=> connect to database');

    if (cachedDb) {
      console.log('=> using cached database instance');
      return Promise.resolve( cachedDb );
    }

    return MongoClient.connect( url, { useNewUrlParser: true } ).then( client => { 

      cachedDb = client.db( DATABASE_NAME );
      console.log('=> using NEW database instance');

      return cachedDb;
    } );
  };

  const asyncHandling = async () => { 
    let database = await Promise.resolve( connect(CONNECTION_URL) );
    return { db: database }; 
  };

  module.exports.handler = asyncHandling;

} )();