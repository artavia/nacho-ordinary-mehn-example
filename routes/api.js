
// =============================================
// BASE SETUP
// =============================================
const express = require('express');
const router = express.Router();

// =============================================
// new boilerplate ~ CRUD
// =============================================

const assert = require('assert');
const objectId = require("mongodb").ObjectID; // !ObjectId 
// const derrpage = (err) => { if( err ){ console.log( "err: ", err ); throw err; }  }; // throw Error 

const initDatabases = require( '../custom_dbs/a-return-database-obj' );

// database specific information & authentication Database's name
const COLLECTION_NAME = "fugitives"; 

// =============================================
// new boilerplate ~ EVERYTHING ELSE
// =============================================
const jwt = require( 'jsonwebtoken' );
const compression = require( 'compression' );  // see https://expressjs.com/en/advanced/best-practice-performance.html 

const { JWT_SECRET } = process.env;

const checkAuth = require( '../custom_middleware/checkAuth' );

const redirectSafe = require( '../custom_middleware/redirectSafe' );
const redirectLogin = require( '../custom_middleware/redirectLogin' );

const Custom_Enable_Content_Transformation_Headers = require( '../custom_middleware/Custom_Enable_Content_Transformation_Headers' );

const Custom_Enable_Content_Security_Policy_Headers = require( '../custom_middleware/Custom_Enable_Content_Security_Policy_Headers' );

// =============================================
// COMPRESSION
// ==============================================
router.use( compression() );

// =============================================
// NEW - middleware galore
// ==============================================

// v1 header authorization
router.use( (req, res, next) => { 
  if( req.session.token ){ 
    const stringutil = `Bearer ${req.session.token}`;
    res.set( 'authorization' , stringutil );
  }
  next();
} );

// v2 header x-access-token
/* router.use( (req, res, next) => { 
  if( req.session.token ){ 
    res.set( 'x-access-token' , req.session.token );
  }
  next();
} ); */

// -----------------------------
// | SECURITY helper functions 
// | optional CSP headers phase(s) ~ https://zinoui.com/blog/security-http-headers 
// | https://wiki.mozilla.org/Security/CSP/Specification
// | and, visit an online CSP header generator at http://cspisawesome.com/ 
// -----------------------------
router.use( Custom_Enable_Content_Security_Policy_Headers );

// ------------------------------
// | PERFORMANCE helper functions 
// | (4) performance-related headers 
// ------------------------------
router.use( Custom_Enable_Content_Transformation_Headers ); // | x Content transformation (1)
// | x Compression (2) -- see server.js <= helmet
// | x ETags (3) -- see server.js <= app.set 
// | Expires headers (4) -- see server.js <= serveStatic


/* ######################################
##  IMPARTIAL TO ANY ROUTE ~ STEP 1  
###################################### */

const genericError = (err) => { console.log( "db promise error --> err: ", err ); } 

const catchStatement = (err) => {console.log( "catchStatement error --> err: ", err ); }

const extractTheClntObj = clientObj => { // console.log( "extractTheClntObj --> clientObj: " , clientObj ); 

  // const res = clientObj.res; // console.log( "extractTheClntObj --> clientObj.res: " , clientObj.res );  
  const db = clientObj.db;   // console.log( "extractTheClntObj --> clientObj.db: " , clientObj.db );  
  return {
    // res: res , 
    db: db 
  };  
};

// ROUTES
// ==============================================
// we'll create our routes here


const apiGET =  ( req, res, next ) => { 
  const { operatorId } = req.session; 
  const { operator } = req.session; 
  res.render( 'authentication' , { message: 'The coolest thing you\'ve seen today!', title: 'Authentication page', operatorId: operatorId, operator: operator } );
};
router.get( '/' , redirectSafe, apiGET ); // apiGET to http://127.0.0.1:3000/api 

const authenticatePOST = ( req, res, next ) => {
  const { operatorname, email } = req.body;
  if( !operatorname || !email ){}
  else
  if( operatorname && email ){
    
    const operator_lite = {
      operatorname: operatorname
      , email: email 
    };

    jwt.sign( { operator: operator_lite } , JWT_SECRET, { expiresIn: '2d' }, ( err, token ) => {
      if(err) throw err;

      req.session.token = token;

      /* res.json( {
        success: true
        , message: 'Enjoy your token!'
        , token: token
      } ); */

      // res.render( 'safe' , { success: true, message: 'Enjoy your token!', title: 'Safe page', operatorId: operatorId, operator: operator, token: token  } );
      
      res.redirect('/api/safe');
      // res.redirect('/api/live');

    } );

  }
};
router.post( '/' , authenticatePOST ); // authenticatePOST to http://127.0.0.1:3000/api/authenticate 




const safeGET =  ( req, res, next ) => {
  const { operatorId } = req.session; 
  const { operator } = req.session; 
  res.render( 'safe' , { message: 'You can use the app freely', title: 'Safe Page', operatorId: operatorId, operator: operator } );
};
router.get( '/safe', redirectLogin , checkAuth , safeGET ); // safeGET to http://127.0.0.1:3000/api/safe 




const liveGET =  ( req, res, next ) => { 

  // so far registerHelper does not work here either...
  
  // IMPARTIAL TO ANY ROUTE                                        -------------------    
  const step_1_return_db = initDatabases.handler().then( extractTheClntObj , genericError );
  
  const step_2_query_find = step_1_return_db.then( db => new_queryDb_find( req, res, db ) ).catch( catchStatement );
  step_2_query_find; 


};
router.get( '/live', redirectLogin , checkAuth , liveGET ); // liveGET to http://127.0.0.1:3000/api/live 

/* ######################################
##  NEW  ~ new_queryDb_find
###################################### */
const new_queryDb_find = ( req, res, myclient ) => {
  
  console.log('=> prepping find all from database'); 
  // console.log( "req: " , req );
  // console.log( "res: " , res );
  
  // const id = req.params.id; 
  // const pm1_filterquery = { "_id" : objectId( id ) }; 

  let resultArray = []; // version two
  const pm1_filterquery = {} ;  
  
  const cursor = myclient.db.collection( COLLECTION_NAME ).find( pm1_filterquery  );
  
  cursor.forEach( 
    
    function( doc, err ){
      if( err ){ console.log( "err: ", err ); throw err; } // derrpage(err);
      assert.equal( null, err );
      resultArray.push( doc );
    } 
    
    , function (){
      
      // client.close();
      // console.log( 'Successfully closed the client connection, too.' ); 

      res.render( "live" 
        , {
          items: resultArray
          , message: 'Live app page', title: 'Live App', operatorId: req.session.operatorId, operator: req.session.operator 
          // NEW TEST
          , showTitle: true
          // , showTitle: false
          
        } 
      );
    } 

  );

};




const livePOST = ( req, res, next ) => {

  // IMPARTIAL TO ANY ROUTE                                        -------------------    
  const step_1_return_db = initDatabases.handler().then( extractTheClntObj , genericError ); // LKGC
  
  // QUERY SPECIFIC STEPS @ 1 FOR AS MANY AS ARE BEING REFERRED TO -------------------  
  // ...  
  const step_2_query_insertone = step_1_return_db.then( db => queryDb_insertOne( req, res, db ) ).catch( catchStatement ); 

  step_2_query_insertone;
  
};
router.post( '/live', redirectLogin, checkAuth, livePOST );

/* ######################################
##  NEW  ~ queryDb_insertOne
###################################### */
const queryDb_insertOne = ( req, res, myclient ) => { 

  console.log('=> prepping insert one to database');
  // console.log( "req: " , req );
  // console.log( "res: " , res );
  // console.log( "myclient: " , myclient );

  const item = {
    nickname: req.body.nickname 
    , name: {
      first: req.body.first
      , last: req.body.last
    }
    , gender: req.body.gender
    , age: req.body.age
    , email: req.body.email
    , location: req.body.location
  };
  // console.log( "=> queryDb_insertOne item: " , item );

  // const cursor = myclient.db.collection( COLLECTION_NAME ).find( {} ); 
  // const collection = cursor.toArray();

  const pm1_insert = item;
  const pm2_insert_cb = function( err, result ) { 
    if( err ){ console.log( "err: ", err ); throw err; } 
    assert.equal( null, err );

    console.log( 'Item inserted into database' );
    // console.log( "result: " , result );
    
    res.redirect('/api/live');

  };

  // const execution = myclient.db.collection( COLLECTION_NAME ).insertOne( pm1_insert, pm2_insert_cb );
  myclient.db.collection( COLLECTION_NAME ).insertOne( pm1_insert, pm2_insert_cb );

};




const deleteGET = ( req, res, next ) => {
  
  const step_1_return_db = initDatabases.handler().then( extractTheClntObj , genericError ); 

  const step_2_query_deleteone = step_1_return_db.then( db => queryDb_deleteOne( req, res, db ) ).catch( catchStatement );

  step_2_query_deleteone;

};
router.get( '/delete/:id', redirectLogin, checkAuth, deleteGET );

/* ######################################
##  NEW  ~ queryDb_deleteOne
###################################### */
const queryDb_deleteOne = ( req, res, myclient ) => {
  
  console.log('=> prepping deletion of one record in database');
  // console.log( "req: " , req );
  // console.log( "res: " , res );
  // console.log( "myclient: " , myclient );

  const id = req.params.id;
  
  var pm1_queryfilter = { "_id" : objectId(id) };
  var pm2_callback = ( err, result ) => {
    if( err ){ console.log( "err: ", err ); throw err; } // derrpage(err); 
    assert.equal( null, err ); 
    console.log( 'Item deleted from database' );
    // console.log( "result: " , result ); 

    // client.close();
    // console.log( 'Successfully closed the client connection, too.' ); 

    res.redirect( '/api/live' ); 
  };
  
  myclient.db.collection( COLLECTION_NAME ).deleteOne( pm1_queryfilter, pm2_callback );

};




const updateGET = ( req, res, next ) => {
  
  const step_1_return_db = initDatabases.handler().then( extractTheClntObj , genericError ); 

  const step_2_query_findone = step_1_return_db.then( db => queryDb_findOne( req, res, db ) ).catch( catchStatement );

  step_2_query_findone;

};
router.get( '/update/:id', redirectLogin, checkAuth, updateGET );

/* ######################################
##  NEW  ~ queryDb_findOne
###################################### */
const queryDb_findOne = ( req, res, myclient ) => {
  
  console.log('=> prepping find one from database'); 
  // console.log( "req: " , req );
  // console.log( "res: " , res );
  
  const id = req.params.id; 

  const pm1_filterquery = { "_id" : objectId( id ) }; 
  
  const pm2_findonecallback =  (err, result) => { 
    if( err ){ console.log( "err: ", err ); throw err; } // derrpage(err);

    assert.equal( null, err );
    console.log( 'Single item fetched from database' );
    // console.log( "result: " , result );
    
    // client.close();
    // console.log( 'Successfully closed the client connection, too.' ); 

    const { operatorId } = req.session; 
    const { operator } = req.session; 

    res.render( "update" 
      , { 
        prospect: result 
        // , thecount: mycount
        , message: 'Updating... whatever!', title: 'Update Page', operatorId: operatorId, operator: operator 
      } 
    );

    
  };

  myclient.db.collection( COLLECTION_NAME ).findOne( pm1_filterquery , pm2_findonecallback );

};




const updatePOST = ( req, res, next ) => {
  
  const step_1_return_db = initDatabases.handler().then( extractTheClntObj , genericError ); 
  
  const step_2_query_updateone = step_1_return_db.then( db => queryDb_updateOne( req, res, db ) ).catch( catchStatement );

  step_2_query_updateone;

};
router.post( '/update/:id', redirectLogin, checkAuth, updatePOST );

/* ######################################
##  NEW  ~ queryDb_updateOne
###################################### */
const queryDb_updateOne = ( req, res, myclient ) => {
  
  console.log('=> prepping update one record in database');
  // console.log( "req: " , req );
  // console.log( "res: " , res );
  // console.log( "myclient: " , myclient );

  const item = {
    nickname: req.body.nickname 
    , name: {
      first: req.body.first
      , last: req.body.last
    }
    , gender: req.body.gender
    , age: req.body.age
    , email: req.body.email
    , location: req.body.location
  };
  
  const id = req.params.id; 

  const pm1_filterquery = { "_id" : objectId(id) };
  const pm2_updatequery = { $set: item };
  const pm3_mongocallback =  (err, result) => { 
    if( err ){ console.log( "err: ", err ); throw err; } // derrpage(err); 
    assert.equal( null, err );
    console.log( 'Item updated in database' );
    // console.log( "result: " , result );

    // client.close();
    // console.log( 'Successfully closed the client connection, too.' ); 

    res.redirect('/api/live');
  }; 
  
  myclient.db.collection( COLLECTION_NAME ).updateOne( pm1_filterquery, pm2_updatequery, pm3_mongocallback );

};




const decodeGET =  ( req, res, next ) => { 
  if(req.authData){
    res.json( req.authData );
  }
  else {
    res.send("Not today. Nope. Not ever.")
  }  
};
router.get( '/decode' , redirectLogin , checkAuth , decodeGET ); // decodeGET to http://localhost:3000/api/decode 



/* ######################################
##  GET home page - OLD as dirt version!
###################################### */

/* router.get( '/live', redirectLogin , checkAuth , function(req, res, next) {
  
  // IMPARTIAL TO ANY ROUTE                                        -------------------    
  const step_1_return_db = initDatabases.handler().then( extractTheClntObj , genericError ); // LKGC
  const step_2_query_find = step_1_return_db.then( db => queryDb_find( db ) ).catch( catchStatement ); // LKGC

  // QUERY SPECIFIC STEPS @ 1 FOR AS MANY AS ARE BEING REFERRED TO ------------------- 
  const step_3_select_table = step_2_query_find.then( obj => establishCollection( obj ) ).catch( catchStatement ); // LKGC  
  const step_4_resolve_the_data = step_3_select_table.then( jabba => pendingPromises( jabba ) ).catch( catchStatement ); // LKGC

  // EVERYTHING IS TIED WITH A PRETTY BOW ON TOP                   -------------------    
  step_4_resolve_the_data.then( blotto => presentTheData( res, blotto ) ); // LKGC
} ); */




/* ######################################
##  QUERY SPECIFIC ~ STEP 2 ~ queryDb_find
###################################### */
// const queryDb_find = ( res, myclient ) => { // LKGC 
// const queryDb_find = ( req, res, myclient ) => { 
const queryDb_find = ( myclient ) => {  

  console.log('=> query database');
  // console.log( "myclient: " , myclient );

  const cursor = myclient.db.collection( COLLECTION_NAME ).find( {} ); // this works, too
  // const cursor = myclient.db.collection( COLLECTION_NAME ).find(); // this works!
  
  // const collection = myclient.db.collection( COLLECTION_NAME ).find( {} ).toArray();
  const collection = cursor.toArray();

  return {
    collection: collection
    // req: req 
    // , res: res 
    // , myclient: myclient
    // , cursor: cursor 
  };
};

/* ######################################
##  QUERY SPECIFIC ~ STEP 3 ~ establishCollection
###################################### */
const establishCollection = async ( pmOne ) => {
  
  // console.log( "pmOne.req" , pmOne.req );
  // console.log( "pmOne.res" , pmOne.res );
  // console.log( "pmOne.myclient" , pmOne.myclient );
  // console.log( "establishCollection --> pmOne.cursor" , pmOne.cursor );  
  // console.log( "establishCollection --> await Promise.resolve( pmOne.collection : " , pmOne.collection );
  
  return {
    pendingCollection: await Promise.resolve( pmOne.collection ) 
    // , req: pmOne.req
    // , res: pmOne.res    
    // , db: pmOne.myclient
    // , cursor: pmOne.cursor
  };
} ;

/* ######################################
##  QUERY SPECIFIC ~ STEP 4 ~  pendingPromises
###################################### */
const pendingPromises = ( obj ) => {
  // console.log( " pendingPromises is ready to party!" );
  // console.log( "obj.res" , obj.res );
  // console.log( "obj.db" , obj.db );
  // console.log( "pendingPromises --> obj.pendingCollection: " , obj.pendingCollection ); 

  return {
    items: obj.pendingCollection
    // , res: obj.res 
    // , req: obj.req
    // , db: obj.db
  };
};

/* ######################################
##  QUERY SPECIFIC ~ STEP 5 ~ presentTheData
###################################### */
const presentTheData = ( res, data) => {
  // console.log( " presentTheData is ready to party!" );  
  // console.log( "Server response object" , data.res );
  // console.log( "Server request object" , data.req );
  // console.log( "data.items" , data.items );

  // const res = data.res;
  res.render( "live" 
    ,  {
      items: resultArray
      , message: 'Live app page'
      , title: 'Live App'
      , operatorId: req.session.operatorId
      , operator: req.session.operator 
      // NEW TEST
      , showTitle: true
      // , showTitle: false 
    } 
  );
  console.log( "=> presentTheData is THROUGH!" );
};

module.exports = router;
