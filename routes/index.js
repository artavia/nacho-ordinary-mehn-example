
// =============================================
// BASE SETUP
// =============================================
const express = require('express');
const router = express.Router();

// =============================================
// new boilerplate
// =============================================
const redirectHome = require( '../custom_middleware/redirectHome' );
const redirectLogin = require( '../custom_middleware/redirectLogin' );

const Custom_Enable_Content_Transformation_Headers = require( '../custom_middleware/Custom_Enable_Content_Transformation_Headers' );

const Custom_Enable_Content_Security_Policy_Headers = require( '../custom_middleware/Custom_Enable_Content_Security_Policy_Headers' );

const compression = require( 'compression' );  // see https://expressjs.com/en/advanced/best-practice-performance.html 
const mongoose = require( 'mongoose' ); // DATABASE OPS 
const OperatorData = require( '../custom_models_mongoose/OperatorData' );
const { check, validationResult } = require( 'express-validator/check'); // EXPRESS-VALIDATOR OPS 
const bcrypt = require('bcrypt');
const saltRounds = 10;

// =============================================
// process.env SETUP
// =============================================
const {  CONNECTION_URL , SESS_NAME = 'sid' } = process.env;

// =============================================
// plus...
// SPRINKLED WITH FUNCTIONS
// DATABASE OPS ... wiring this up to mongodb NOW!
// =============================================
mongoose.connection.openUri( CONNECTION_URL , { useNewUrlParser: true } );

/* ######################################
##  CONVENIENCE FUNCTIONS ~ IMPARTIAL TO ANY ROUTE ~ STEP 1  
###################################### */
// const genericError = (err) => { console.log( "db promise error --> err: ", err ); } 
const catchStatement = (err) => {console.log( "catchStatement error --> err: ", err ); }

// =============================================
// COMPRESSION
// ==============================================
router.use( compression() );


// -----------------------------
// | SECURITY helper functions 
// | optional CSP headers phase(s) ~ https://zinoui.com/blog/security-http-headers 
// | https://wiki.mozilla.org/Security/CSP/Specification
// | and, visit an online CSP header generator at http://cspisawesome.com/ 
// -----------------------------
router.use( Custom_Enable_Content_Security_Policy_Headers );

// ------------------------------
// | PERFORMANCE helper functions 
// | (4) performance-related headers 
// ------------------------------
router.use( Custom_Enable_Content_Transformation_Headers ); // | x Content transformation (1)

// | x Compression (2) -- see server.js <= helmet
// | x ETags (3) -- see server.js <= app.set 
// | Expires headers (4) -- see server.js <= serveStatic

// =============================================
// ROUTES
// ==============================================
// we'll create our routes here

const logoutPOST = ( req, res, next ) => {
  
  req.session.destroy( err => {
    if( err ) return res.redirect('/home'); 
    res.clearCookie( SESS_NAME );
    res.redirect('/login');
  } );

};
router.post( '/logout' , redirectLogin, logoutPOST ); // POST /logout 

const methodology_GET = ( req, res, next ) => { 
  res.render( 'methodology', { title: 'Methodology page' } );
};
router.get( '/methodology' , methodology_GET ); // GET / 

const is_sessionGET = ( req, res, next ) => { 
  const words = ['ExpressJS', 'Mongo-Atlas', 'Heroku Client' ];  
  const { operatorId } = req.session; 
  const { operator } = req.session;
  res.render( 'is-session', { words: words, title: 'Index page', operatorId: operatorId, operator: operator } );
};
router.get( '/' , is_sessionGET ); // GET / 



const homeGET =  ( req, res, next ) => { 
  const { operator } = req.session;
  res.render( 'home', { title: 'Home page', operator: operator } );
};
router.get( '/home' , redirectLogin, homeGET ); // GET /home 



const loginGET = ( req, res, next ) => { 
  res.render( 'login' , { title: 'Login page' } );
};
router.get( '/login' , redirectHome, loginGET); // GET /login 




const filterOneResult = ( email , operators ) => {
  // console.log( operators , operators );
  let value;
  let theoperator;
  const doesEmailExist = operators.some( operator => operator.email === email );
  
  if( !doesEmailExist ){
    value = false;
  }
  if( !!doesEmailExist ){
    value = true;
    theoperator = operators.filter( (operator) => {
      return ( operator.email === email );
    } )
  }
  
  return {
    value: value
    , operatorarray: theoperator
  }; // return value;
};

const momentOfTruth = ( plntxtPdub, req, res, data ) => {
  
  let returnedOutcome;
  if( data.value === false ){
    returnedOutcome = res.render( 'login' , { message: 'Go register! That information is not in our records.' } );
  }
  if( data.value === true ){

    let {operatorarray} = data; 
    // console.log( "my operatorarray!!! " , operatorarray );
    let operator = operatorarray[0]; 

    if( !bcrypt.compareSync( plntxtPdub , operator.password ) ){
      // console.log( '===>  No match' );
      returnedOutcome = res.render( 'login' , { message: 'The password is invalid. Again, please.' } );
    } 
    if( !!bcrypt.compareSync( plntxtPdub , operator.password ) ){ 
    // if( !!bcrypt.compareSync( req.body.password , operator.password ) ){

      // console.log( '===>  We have a match!' );

      req.session.operatorId = operator._id;
      req.session.operator = operator;
      
      // returnedOutcome = res.status(200).send("Hold on, please!");
      returnedOutcome = res.redirect( '/home' );
    }
    
  }
  return returnedOutcome;
};


const loginPOST = ( req, res, next ) => { 
  try{
    let returnedVal;

    const { email , password } = req.body;
    
    if( !email || !password ){
      returnedVal = res.redirect('/login');
    }
    if( !!email && !!password ){
      const findallpromise = fetchAsyncRetAll_Multi();
      const step_two = findallpromise.then( operators => filterOneResult( email , operators ) ).catch( catchStatement );
      const step_three = step_two.then( obj => momentOfTruth( password, req, res, obj ) ).catch( catchStatement );
      returnedVal = step_three;
    }

    return returnedVal;
    
  } catch (derrp){ 
    // console.log("error: " , derrp );
    // console.log( 'Operator and/or password are wrong!'  ); 
    // return 
    res.render( 'login' , { message: 'Operator and/or password are wrong. Again, please.' } ); 
    // res.status(500).send(derrp);
  }
  
};

router.post( '/login' , redirectHome, loginPOST ); // POST /login


const registerGET = ( req, res, next ) => {
  res.render( 'register' , { title: 'Register page' } );
};
router.get( '/register' , redirectHome, registerGET ); // GET /register 



const handlersArray = [ 
  check('operatorname').isLength( { min: 2 } ) // operatorname must be at least 2 chars long 
  , check('email').isEmail() // email must be an email 
  , check('password').isLength( { min: 4 } ) // password must be at least 4 chars long 
];

const fetchAsyncRetAll_Multi = async () => {
  const promise = await OperatorData.find( {} ).exec();
  return promise;
};

const registerPOST = ( req, res, next ) => { 

  try{
    const { operatorname, email , password } = req.body;
    
    if( !operatorname || !email || !password ){
      return res.redirect( '/register' );   // TODO querystring errors  ... COMPLETE
    }
    
    const findallpromise = fetchAsyncRetAll_Multi(); 

    const step_two = findallpromise.then( operators => doesOperatorExist( req, operators ) ).catch( catchStatement );

    const step_three = step_two.then( data => stopOrGo( req, res, data ) ).catch( catchStatement );
    step_three;

  } catch (derrp){
    res.status(500).send(error);
  }
};
router.post( '/register' , handlersArray, redirectHome, registerPOST ); // POST /register 

const doesOperatorExist = ( req, operators ) => {  
  
  let value;
  // console.log( "operators: " , operators ); 
  const doesEmailExist = operators.some( operator => operator.email === req.body.email );
  const doesTheNameExist = operators.some( operator => operator.operatorname === req.body.operatorname );

  if( doesEmailExist || doesTheNameExist ){
    // console.log( "Stop. You have to cease at this time." );
    value = true;
  } 
  if( !doesEmailExist && !doesTheNameExist ){
    value = false;
  }
  return value;
};

const fetchAsyncInsertOne = async ( obj ) => {
  
  const data = new OperatorData(obj);
  await data.save( (err) => {
    if(err) throw err;
    // console.log("Item has been added successfully!");
    // res.json( {success: true} );
  } ); 

};

const stopOrGo = (req, res, data) => { 
  
  // console.log( 'data: ', data );

  if( data === true ){
    return res.render( 'register' , { message: 'Operator already exists. Again, please.' } );
  }
  else
  if( data === false ){

    const validationErrors = validationResult( req );
    
    if( !validationErrors.isEmpty() ){

      // console.log( 'validationErrors.array(): ' , validationErrors.array() );
      // return res.status(422).json( { errors: validationErrors.array() } );

      return res.render( 'register' , { message: 'There were errors with your form submission.' , errors: validationErrors.array() } );
    }

    // continue
    const { operatorname, email , password } = req.body; 
     // sync version
    var hash = bcrypt.hashSync( password, saltRounds );
    const operator = {
      operatorname: operatorname
      , email: email
      // , password: password
      , password: hash 
    }; 
    
    fetchAsyncInsertOne( operator );
    // console.log("Item has been added successfully!");


    return res.redirect('/login');
  }

};

module.exports = router;