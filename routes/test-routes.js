// console.log("hello, friend\r\n");

const {
  FOO
  , NODE_ENV
  , CONNECTION_URL
} = process.env;

// ROUTES
// ==============================================
// we'll create our routes here
const express = require('express');
const router = express.Router(); // get an instance of router

// route middleware that will happen on every request
const everyrequestcallback = ( req, res, next ) => {
  // log each request to the console
  // console.log( 'req.method: ' , req.method );
  // console.log( 'req.url: ', req.url );  
  next(); // go to the next thing
};
router.use( everyrequestcallback );

router.get( '/' , ( req, res, next ) => {
  
  console.log( "FOO: " , FOO ); 
  console.log( "NODE_ENV: " , NODE_ENV ); 
  console.log( "CONNECTION_URL: ", CONNECTION_URL );
  // console.log( "process.env: " , process.env );

  const message = `This is the home page`;
  res.send( message );
  
} ); // home page route (http://localhost:3000)

router.get( '/about' , ( req, res, next ) => {
  const message = `This is the about page`;
  res.send( message );
} ); // about page route (http://localhost:3000/about)

// route middleware for parameters
// route middleware to validate :name
const paramcallback = ( req, res, next, name ) => {
  
  // do validation on name here...
  // log something so we know its working
  console.log('doing name validations on ' + name);

  // ...once validation has been performed save the new item in the req
  req.name = name;
  next(); // go to the next thing
};
router.param( 'name' , paramcallback );

// parameterized route request
router.get( '/hello/:name' , ( req, res, next ) => {
  const message = `Hello, ${ req.params.name}!`;
  res.send( message );
} );

module.exports = router;