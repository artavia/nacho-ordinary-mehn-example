
var Handlebars = require('handlebars');

module.exports = Handlebars.registerHelper( { 

  mycaps: (str) => { 
    return new Handlebars.SafeString( str.toUpperCase() );
  } 
  
  /**/ , copyrightYear: () => {
    var year = new Date().getFullYear();
    return new Handlebars.SafeString(year);
  }
  
  , dateLocal: () => {
    var year = new Date().toLocaleDateString();
    return new Handlebars.SafeString( year );
  } /**/

  , safeLinks: ( pmData ) => { 
    // console.log( '=> in the app-level registerHelper: ' , pmData );
    const result = `<div>
      <p>
        <nav>
          <a href="/api/update/${ pmData }">Update</a>&nbsp;
          &nbsp;
          <a href="/api/delete/${ pmData }">Delete</a>&nbsp;
        </nav>
      </p>
    </div>`;
    return new Handlebars.SafeString( result ); 
  }

  , prettifyTheDates: ( sightings ) => {
    // console.log( '=> in the app-level registerHelper: ' , sightings ); 
    // console.log( sightings.at );      // date object denoting login time
    // console.log( sightings.minutes ); // number denoting length of session 
    return new Handlebars.SafeString( sightings.at );
  }

} );