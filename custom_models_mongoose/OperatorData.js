
const COLLECTION_NAME = "operators"; 
const mongoose = require( 'mongoose' );

let OperatorDataSchema = new mongoose.Schema( {
  operatorname: String 
  , email: String
  , password: String 
} , {
  collection: COLLECTION_NAME 
} );

const OperatorData = mongoose.model( 'OperatorData' , OperatorDataSchema );
module.exports = OperatorData;