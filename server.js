
// new boilerplate
// =============================================
const mongoose = require( 'mongoose' ); // DATABASE OPS 
const expressValidator = require( 'express-validator'); // EXPRESS-VALIDATOR OPS 
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

// const helmet = require( 'helmet' ); // https://expressjs.com/en/advanced/best-practice-security.html

const exphbs = require( 'express-handlebars' );
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const createError = require('http-errors');
const serveStatic = require('serve-static');
const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');


// BASE SETUP
// =============================================
const express = require( 'express' );
const app = express(); 

// =============================================
// process.env SETUP
// =============================================
// const PORT = process.env.PORT || 3000; // const { PORT } = process.env;

const { 
  PORT
  , SESS_NAME = 'sid'
  , SESS_LIFETIME = 1000 * 60 * 60 * 2 // 2 HOURS
  , SESS_SECRET 
} = process.env;




// ----------------------------------------------------------------------
// | helmet                                                              |
// ----------------------------------------------------------------------
// app.use( helmet() ); 
// app.use( helmet.noCache() ); // https://helmetjs.github.io/docs/nocache/ 

// ----------------------------------------------------------------------
// | trust proxy setting                                                |
// | this permits the assignment of secure cookies which is choking
// | at the moment because of express-session
// ----------------------------------------------------------------------
app.enable( 'trust proxy' , 1 );

// ----------------------------------------------------------------------
// | Server-side technology information                                 |
// | Remove the `X-Powered-By` response header that
// | contributes to header bloat and can unnecessarily expose vulnerabilities
// ----------------------------------------------------------------------
app.disable( 'x-powered-by' );


// ----------------------------------------------------------------------
// | ETags                                                              |
// | PERFORMANCE helper function
// | Remove `ETags` as resources are sent with far-future expires headers
// | https://developer.yahoo.com/performance/rules.html#etags
// ----------------------------------------------------------------------
app.set( 'etag' , false );


// new boilerplate
// =============================================
// SESSION
// ==============================================
// we'll configure our sessions here...
const IN_PROD = process.env.NODE_ENV === 'production';

const sessionConfigObj = {

  name: SESS_NAME
  , resave: false 
  , saveUninitialized: false
  , secret: SESS_SECRET

  , store: new MongoStore( {
    mongooseConnection: mongoose.connection
    // , ttl: 14 * 24 * 60 * 60 // = CHANGED FROM 14 days Default (this is in seconds)
    , ttl: 2 * 24 * 60 * 60 // = CHANGED TO 2 days (this is in seconds)
  } )

  , cookie: {
    maxAge: SESS_LIFETIME 
    , sameSite: true // 'strict'
    , secure: IN_PROD
  }

};

app.use( session( sessionConfigObj ));


// view engine setup
// app.set('views', path.join(__dirname, 'views')); // new boilerplate 
app.engine( 'hbs', exphbs( { 
  extname: 'hbs' , 
  layoutsDir: path.join( __dirname , "views" ) , 
  partialsDir: path.join( __dirname , "views/partials" ) , 
  defaultLayout: 'layout' ,  
  helpers: require( './custom_hb_helpers/helper-functions' )
} ) ); // NEW

app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json()); // for parsing application/json
// app.use(express.urlencoded({ extended: false })); // no reason for this to be on...
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use( expressValidator() );

app.use(cookieParser());

app.use( express.static( path.join( __dirname, 'public') ) );

// ----------------------------------------------------------------------
// | Expires headers                                                    |
// | PERFORMANCE helper function
// | https://gist.github.com/tagr/170627486377551831de
// | https://devcenter.heroku.com/articles/increasing-application-performance-with-http-cache-headers
// ----------------------------------------------------------------------

const newCustom_Enable_Expires_Headers = ( res, path ) => {
  
  if( ( serveStatic.mime.lookup( path ) === 'text/cache-manifest' ) || ( serveStatic.mime.lookup( path ) === 'application/xml' ) || ( serveStatic.mime.lookup( path ) === 'application/json' ) ){  // zero seconds
    // res.set( 'Cache-Control' , 'private, max-age=0' );
    var secs = 0;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 0 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  } 
  else 
  if( ( serveStatic.mime.lookup( path ) === 'application/rss+xml' ) ){ // 1 hour (86400 / 24)  
    // res.set( 'Cache-Control' , 'private, max-age=3600' );
    var secs = 3600;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 3600000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }
  
  else 
  if( ( serveStatic.mime.lookup( path ) === 'image/x-icon' ) ){ // 1 week (86400 * 7)  
    // res.set( 'Cache-Control' , 'private, max-age=604800' );
    var secs = 604800;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 604800000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }
  else

  if( ( serveStatic.mime.lookup( path ) === 'font/woff' ) || ( serveStatic.mime.lookup( path ) === 'font/woff2' ) || ( serveStatic.mime.lookup( path ) === 'application/vnd.ms-fontobject' ) || ( serveStatic.mime.lookup( path ) === 'application/font-sfnt' ) || ( serveStatic.mime.lookup( path ) === 'application/octet-stream' ) || ( serveStatic.mime.lookup( path ) === 'video/x-f4v' ) || ( serveStatic.mime.lookup( path ) === 'audio/mp4a-latm' ) || ( serveStatic.mime.lookup( path ) === 'audio/ogg' ) || ( serveStatic.mime.lookup( path ) === 'video/mp4' ) || ( serveStatic.mime.lookup( path ) === 'video/ogg' ) || ( serveStatic.mime.lookup( path ) === 'video/webm' ) || ( serveStatic.mime.lookup( path ) === 'image/bmp' ) || ( serveStatic.mime.lookup( path ) === 'image/gif' ) || ( serveStatic.mime.lookup( path ) === 'image/jpeg' ) || ( serveStatic.mime.lookup( path ) === 'image/png' ) || ( serveStatic.mime.lookup( path ) === 'image/svg+xml' ) || ( serveStatic.mime.lookup( path ) === 'image/webp' )  ){ // 30 days (86400 * 30 )
    
    // res.set( 'Cache-Control' , 'private, max-age=2592000' );
    var secs = 2592000;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 2592000000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }

  // webm|bmp|gif|jpg|jpeg|png|svgz?|webp

  else 
  if( ( serveStatic.mime.lookup( path ) === 'text/css' ) || ( serveStatic.mime.lookup( path ) === 'application/javascript' ) ){
    
    // res.set( 'Cache-Control' , 'private, max-age=31536000' );
    var secs = 31536000;
    var expr = 'public, max-age='+ '' + secs +'';
    res.set( 'Cache-Control' , expr );
    // res.set( 'Expires', new Date(Date.now() + 31536000000 ).toUTCString() );
    res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 
  }

};

app.use( serveStatic( path.join( __dirname , 'public' ) , {
  setHeaders: newCustom_Enable_Expires_Headers
} ) );

// apply the routes to our application

app.use( '/' , indexRouter );
app.use( '/api' , apiRouter );

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// START THE SERVER
// ==============================================

// app.listen( PORT, '127.0.0.1', null, () => {
  // console.log( 'Content is being served on PORT ' + PORT );
  // console.log( `Content is served. Find it at http://127.0.0.1:${PORT} . ` );
// } ); // PORT, hostname, backlog, cb

app.listen( PORT, () => {
  console.log( 'Content is being served on PORT ' + PORT );
  // console.log( `Content is served. Find it at http://127.0.0.1:${PORT} . ` );
} ); // PORT, cb

// based on ... https://scotch.io/tutorials/learn-to-use-the-new-router-in-expressjs-4 