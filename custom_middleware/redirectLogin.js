module.exports = ( req, res, next ) => {
  if( !req.session.operatorId ){
    res.redirect( '/login' );
  }
  else {
    next();
  }
};