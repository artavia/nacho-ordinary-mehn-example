module.exports = ( req, res, next ) => {
  if( !!req.session.token ){
    res.redirect( '/api/safe' );
  }
  else {
    next();
  }
};