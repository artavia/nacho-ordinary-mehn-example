const jwt = require( 'jsonwebtoken' );

const {
  JWT_SECRET
} = process.env;

module.exports = ( req, res, next ) => {  
  try{ 
    if( req.session.token ){ 

      // v0 
      // const token = req.session.token;

      // v1
      /* const token = req.body.token || req.query.token || req.headers['x-access-token'];
      // decode token
      if (token) { 
        console.log( 'token: ' , token );
        
        // verifies secret and checks exp
        
        // let newtoken = token + 'assy';
        // jwt.verify( newtoken, JWT_SECRET , (err, authData) => { 
        jwt.verify( token, JWT_SECRET , (err, authData) => {
          if (err) { // return 
            res.json({ success: false, message: 'Failed to authenticate token.' }); 
          } 
          else { // if everything is good, save to request for use in other routes
            req.authData = authData;  // console.log( 'authData: ' , authData );
          }
        } );
      } */

      // v2
      const token = req.body.token || req.query.token || req.headers[ 'authorization' ];
      // decode token
      if (token) { 
        console.log( 'token.split( " " )[1]: ' , token.split( " " )[1] );
        
        // verifies secret and checks exp
        
        // let newtoken = token + 'assy';
        // jwt.verify( newtoken, JWT_SECRET , (err, authData) => { 
        jwt.verify( token.split( " " )[1], JWT_SECRET , (err, authData) => {
          if (err) { // return 
            res.json({ success: false, message: 'Failed to authenticate token.' }); 
          } 
          else { // if everything is good, save to request for use in other routes
            req.authData = authData;  // console.log( 'authData: ' , authData );
          }
        } );
      }

      next();
    }

  } catch( error ){
    
    // return res.status(401).json( { 
    return res.status(401).render( 'error' , {
      // message: 'Auth has failed.'// 401
      message: 'No token provided.' // 403
    } );

  }
};