module.exports = ( req, res, next ) => {
  if( !!req.session.operatorId ){
    res.redirect( '/home' );
  }
  else {
    next();
  }
};