# Description
Integrating best practices in Heroku CLI, Mongo-Atlas, ExpressJS in the same context for maximum UX without the drag.

## Addendum Number One with Justification
This project has been updated for use subsequent to December 2022. I had five (5) projects simultaneously go to the pot given that Heroku brought Stack 18 to [EOL as explained here](https://help.heroku.com/X5OE6BCA/heroku-18-end-of-life-faq "link to Heroku Help"), hence, a slight re-working of said projects (this is no exception).

## Updated status (2023) ~~Please visit~~
Sorry but subsequent to December 2022 and as a result of the new policy at SalesForce/Heroku, free plans are no longer available to the general public. Thus, the site will be left in an inoperable state. ~~You can see [the lesson at heroku](https://piratecrud-demo.herokuapp.com/ "the lesson at heroku") and decide if this is something for you to play around with at a later date.~~

## Final Addendum (2023)
This project utilized Heroku. And it will be currently mothballed as a result of *limited resources*. The five affected projects in question are:
  - [marvel-demo-two-crypto-module-version](https://gitlab.com/artavia/marvel-demo-two-crypto-module-version "link to Gitlab Repository");
  - [marvel-demo-three-webpack-version](https://gitlab.com/artavia/marvel-demo-three-webpack-version "link to Gitlab Repository");
  - [issuetracker-mean-demo](https://gitlab.com/artavia/issuetracker-mean-demo "link to Gitlab Repository");
  - [todotracker-mern-demo](https://gitlab.com/artavia/todotracker-mern-demo "link to Gitlab Repository");
  - [nacho-ordinary-mehn-example](https://gitlab.com/artavia/nacho-ordinary-mehn-example "link to Gitlab Repository");

## Yeah, boy!!!
In short, it comes stoked with lots of packages for different types of operations.
  - mongodb@3.1.13
  - assert@1.4.1
  - mongoose@5.4.19
  - concurrently@4.1.0
  - bcrypt@3.0.4   

## Inherit presumptions about you
You have each **MongoDB**, **Yarn**, **Nodemon** installed locally and (eventually) will move on to each **Mongo Atlas** and **Heroku**.

## The biggest challenge(s)
Two words: **trust proxy**

## Some small advice
During development with local database instances (if you desire), I have formulated a contrived setup found mainly in each the **apps/PROJECTNAME** and **data-project/PROJECTNAME/data** folders.

You will need between two and three terminals (all opened from your **Present Working Directory**) in order to operate the application!

From any of the terminals - delete and recreate DB instance
```sh
$ yarn run culldata
```

Terminal 1 - Initiate Mongo (until it becomes unnecessary)
```sh
$ mongod --dbpath $PWD/data-project/PROJECTNAME/data --port 27017
```

Terminal 2 - Run the Mongo Client
```sh
$ mongo 127.0.0.1/PROJECTNAME $PWD/apps/PROJECTNAME/EXAMPLE.js
$  [OR...]
$ mongo 127.0.0.1/PROJECTNAME
$  [OR...]
$ mongo PROJECTNAME
```

Terminal 3 - Run the Express App with Yarn
```sh
$ yarn run local 
```

## You want to know what&rsquo;s &quot;under the hood,&quot; right?
The server includes performance boosting directives and security precautions
  - The `X-Powered-By` response header is removed, and ETags are neutralized
  - Each the `Cache-Control` and `Expires` headers are applied (based on extension) for future caching

The routes (depending on the portion of the website we are focused on) include minor enhancements, too
  - All extensions that the app overlooked are now infused with their own `Pragma` and `Expires` headers set to 'no&#45;cache', plus, the obligatory `Cache-Control` header to set all other items to **zero (0) milliseconds**
  - Some minimal application of Content-Security-Policy to our routes in order to mitigate any pesky intrusion attempts from outside of our own domain

I will wager that byte for byte, you cannot get much a **safer** or **faster** experience than what has been presented for you today. I hope you enjoy your visit.

## How I eventually rolled my own&hellip;
In short, I focused on my boilerplate in this order and utilized the following:
  - You need to have nodemon installed globally (this is the first presumption);
  - Each a **config** and a **dev** run&#45;script to address initial environment variables whether its one or the other;
  - helmet&amp; compression: self&#45;explanatory in the sense that these partially will address best practices in ExpressJS;
  - express-handlebars: for Handlebars customizations configuration;
  - bootstrap, jquery &amp; popper.js: for something pretty while establishing default styling boilerplate (I LIKE IT!);
  - async/await functions: I saw in each the ["best practices for performance guide"]( "https://expressjs.com/en/advanced/best-practice-performance.html") and at each the ["mongo blog"]("https://www.mongodb.com/blog/post/optimizing-aws-lambda-performance-with-mongodb-atlas-and-nodejs") the ["mongo docs"]("https://docs.atlas.mongodb.com/best-practices-connecting-to-aws-lambda/#example");
  - strategically placed a CONNECTION_URL env variable into workflow (as opposed to being tucked into the package.json);

Next, I had to focus on two new additions to my original work. My original work consisted of a CRUD app sans any method of authentication built in. Presently, and for anyone who wants to bother, anybody can choose to sign up and &quot;play along.&quot; Thus, several changes were warranted and included 1) a new collection of &quot;operators&quot; and (ended up not) renaming the other collection from &quot;users&quot; to &quot;fugitives&quot;. Here is what it does: 
  - work with express-session ( as an alternative use jsonwebtoken instead );
  - save /signup form contents to NEW db;
  - execute /login form contents asynchronously with NEW db;
  - once authentication has been established, said &quot;admin&quot; can use the CRUD application unencumbered; 
  - perform basic input validation with express-validator; 
  - perform basic password hashing/comparison and possibly some basic operatorname urlencoding/decoding; 
  - leave option open for including &quot;white-and-black-listing functionalities&quot;;
  - Utilize all forms of routing tricks and more!


## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!

